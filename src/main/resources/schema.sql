drop table book if exists;
drop sequence if exists book_sequence;

create sequence book_sequence start with 1 increment by 1;
create table book (id bigint not null, author varchar(255), format varchar(255), name varchar(255), price double not null, primary key (id))